import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class ItemIDCollector {

    public static void main(String[] args) {
        String line;
        Set<String> SellerIdsList = new HashSet<>();
        try {
            FileInputStream fileInputStream = new FileInputStream("c:\\JAVA\\SellerIDCollector.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while ((line = bufferedReader.readLine()) != null) {
                SellerIdsList.add(line);
            }
            bufferedReader.close();
            System.out.println(SellerIdsList.size());
            FileOutputStream fileOutputStream = new FileOutputStream("c:\\JAVA\\ItemIDCollector.txt");
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter, 500000);
            for (String each : SellerIdsList) {
                // http://molotok.ru/show_user.php?uid=29390825&feedback_type=fb_recvd_all&type=fb_seller&p=2
                System.out.println("SellerIDCollector = " + each);
                List<String> itemsIdList = getIdList(each);
                for (String each2 : itemsIdList) {
                    System.out.println("\t\t" + "ItemIDCollector = " + each2);
                    bufferedWriter.write(each2 + "\r\n");
                }
                bufferedWriter.flush();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> getIdList(String url) {

        List<String> itemIdList = new ArrayList<>();
        int pages = 1;
        url = "http://molotok.ru/show_user.php?uid=" + url + "&feedback_type=fb_recvd_all&type=fb_seller&p=";
        String html = getPageHtml(url);
        if (!html.contains("Последние 30 дней")) {
            // System.out.println("Последние 30 дней");
            return itemIdList;
        }
        // <li class="suffix">/</li>
        // <li><a href="/show_user.php?uid=29390825&feedback_type=fb_recvd_all&type=fb_seller&p=17">17</a></li>

        int firstIndex = 0;
        int middleIndex = 0;
        int lastIndex = 0;
        String firstToken = "";
        String middleToken = "";
        String lastToken = "";
        
        firstToken = "<span class=\"user\">";
        middleToken = "<span class=\"uname\">";
        lastToken = "</span>";
        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        middleIndex = html.indexOf(middleToken, firstIndex) + middleToken.length();
        lastIndex = html.indexOf(lastToken, middleIndex);
        //String user = html.substring(middleIndex, lastIndex);
        
        firstToken = "<span class=\"user-rating\">(";
        lastToken = ")>";

        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        lastIndex = html.indexOf(lastToken, firstIndex);
        //String rating = html.substring(firstIndex, lastIndex);
        
        
        firstToken = "<li class=\"suffix\">/</li>";
        middleToken = "feedback_type=fb_recvd_all&type=fb_seller&p=";
        lastToken = "\"";

        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        middleIndex = html.indexOf(middleToken, firstIndex) + middleToken.length();
        lastIndex = html.indexOf(lastToken, middleIndex);

        String pagesText = html.substring(middleIndex, lastIndex);
        pages = Integer.parseInt(pagesText);

        for (int i = 1; i <= pages; i++) {

            System.out.println("\t" + url + i);
            html = getPageHtml(url + i);

            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!html.contains("Последние 30 дней")) {
                // System.out.println("Последние 30 дней");
                return itemIdList;
            }
            firstIndex = 0;
            middleIndex = 0;
            lastIndex = 0;
            firstToken = "";
            middleToken = "";
            lastToken = "";

            firstToken = "<table id=\"feedbacks-list\" class=\"fb-listing table table-two-rows-striped\">";
            lastToken = "</table>";

            firstIndex = html.indexOf(firstToken, lastIndex);
            lastIndex = html.indexOf(lastToken, firstIndex);
            html = html.substring(firstIndex, lastIndex);

            firstIndex = 0;
            middleIndex = 0;
            lastIndex = 0;
            firstToken = "";
            middleToken = "";
            lastToken = "";

            while (true) {

                // поиск Id
                firstToken = "<span class=\"pos\">Хорошо</span>";
                middleToken = "<a href=\"/item";
                lastToken = "_";
                firstIndex = html.indexOf(firstToken, lastIndex);

                if (firstIndex < 0) {
                    break;
                }
                firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
                middleIndex = html.indexOf(middleToken, firstIndex) + middleToken.length();
                lastIndex = html.indexOf(lastToken, middleIndex);
                String idText = html.substring(middleIndex, lastIndex);
                itemIdList.add(idText);

            }
        }
        return itemIdList;
    }

    public static String getPageHtml(String url) {
        String html = "";
        try {
            URL siteUrl = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) siteUrl.openConnection();
            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream, "UTF-8");
            html = scanner.useDelimiter("\\A").next();
            scanner.close();
        } catch (IOException e) {
            System.out.println("Connection timeout for " + url);
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            getPageHtml(url);
        } catch (NoSuchElementException e) {
            System.out.println(new Date());
            System.out.println("Connection ban for " + url);
            int sleepTime = new Random().nextInt(6);
            System.out.println("Sleep for " + sleepTime + " seconds");
            try {
                TimeUnit.SECONDS.sleep(sleepTime);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            System.out.println("Retrying ...");
            getPageHtml(url);
        }

        return html;
    }

}
