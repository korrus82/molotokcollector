import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class SellerIDCollector {
    public static void main(String[] args) {

        try {
            FileOutputStream fileOutputStream = new FileOutputStream("c:\\JAVA\\SellerIDCollector_vse-ostalnoe.txt");
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter, 500000);

            long start = System.currentTimeMillis();
//            List<String> categoriesBigList = getBigCategoriesList("http://molotok.ru/");
//            for (String each : categoriesBigList) {
//                System.out.println(each);

                List<String> categoriesList = getCategoriesList("http://molotok.ru/otdel/vse-ostalnoe");
                for (String each2 : categoriesList) {
                    System.out.println("\t" + each2);

                    Set<Integer> idList = getIdList(each2);

                    for (Integer each3 : idList) {
                        idList.add(each3);
                        System.out.println("\t\t\t" + each3);
                        bufferedWriter.write(each3 + "\r\n");
                    }
                    bufferedWriter.flush();
                }
//            }
            long end = System.currentTimeMillis();
            System.out.println("parser =  " + (end - start) / 1000 / 60 + "min");
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Set<Integer> getIdList(String url) {

        Set<Integer> sellerIdsList = new HashSet<>();
        int pages = 1;
        // if (!url.equals("http://molotok.ru/mat-i-ditya?limit=180&order=td&p=")) {
        // continue;
        // }
        String html = getPageHtml(url);

        // <div id="pager-top" class="pager pager-top" rel="ЧИСЛО СТРАНИЦ">
        int firstIndex = 0;
        int middleIndex = 0;
        int lastIndex = 0;
        String firstToken = "";
        String middleToken = "";
        String lastToken = "";
        firstToken = "<div id=\"pager-top\" class=\"pager pager-top\" rel=\"";
        lastToken = "\">";

        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        lastIndex = html.indexOf(lastToken, firstIndex);
        String pagesText = html.substring(firstIndex, lastIndex);
        pages = Integer.parseInt(pagesText);
        for (int i = 1; i <= pages; i++) {

            System.out.println("\t\t\t" + url + i);
            // if (!url.equals("http://molotok.ru/mat-i-ditya?limit=180&order=td&p=") && i != 1) {
            // continue;
            // }
            html = getPageHtml(url + i);
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!html.contains("cписок предложений")) {
                continue;
            }
            firstIndex = 0;
            middleIndex = 0;
            lastIndex = 0;
            firstToken = "";
            middleToken = "";
            lastToken = "";

            firstToken = "<section class=\"offers\"";
            lastToken = "<div class=\"listing-options-bottom\">";

            firstIndex = html.indexOf(firstToken, lastIndex);
            lastIndex = html.indexOf(lastToken, firstIndex);
            html = html.substring(firstIndex, lastIndex);
            firstIndex = 0;
            middleIndex = 0;
            lastIndex = 0;
            firstToken = "";
            middleToken = "";
            lastToken = "";

            while (true) {

                // поиск Id
                firstToken = "data-hidetooltip='' data-popup=";
                middleToken = "\"s\":\"";
                lastToken = "\"";
                firstIndex = html.indexOf(firstToken, lastIndex);

                if (firstIndex < 0) {
                    break;
                }
                firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
                middleIndex = html.indexOf(middleToken, firstIndex) + middleToken.length();
                lastIndex = html.indexOf(lastToken, middleIndex);
                String idText = html.substring(middleIndex, lastIndex);
                int id = Integer.parseInt(idText);
                sellerIdsList.add(id);

            }
        }
        return sellerIdsList;
    }

    private static List<String> getCategoriesList(String url) {
        List<String> list = new ArrayList<String>();
        String html = getPageHtml(url);
        int firstIndex = 0;
        int middleIndex = 0;
        int lastIndex = 0;
        String firstToken = "";
        String middleToken = "";
        String lastToken = "";

        firstToken = "<section id=\"category-map\" class=\"category-map separator-top\">";
        lastToken = "</section>";

        firstIndex = html.indexOf(firstToken, lastIndex);
        lastIndex = html.indexOf(lastToken, firstIndex);
        html = html.substring(firstIndex, lastIndex);

        firstIndex = 0;
        middleIndex = 0;
        lastIndex = 0;
        firstToken = "";
        middleToken = "";
        lastToken = "";

        while (true) {
            // поиск href

            firstToken = "<h3>";
            middleToken = "<a href=\"";
            lastToken = "\"";
            firstIndex = html.indexOf(firstToken, lastIndex);

            if (firstIndex < 0) {
                break;
            }
            firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
            middleIndex = html.indexOf(middleToken, firstIndex) + middleToken.length();
            lastIndex = html.indexOf(lastToken, middleIndex);
            String hrefText = html.substring(middleIndex, lastIndex);
            if (hrefText.equals("http://molotok.ru/tovary-dlya-vzroslyh?ref=category-tree")) {
                continue;
            }
            // поиск названию ссылки

            firstToken = "<span>";
            lastToken = "</span>";
            firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
            lastIndex = html.indexOf(lastToken, firstIndex);
            String linkText = html.substring(firstIndex, lastIndex);
            linkText = linkText.trim();
            hrefText = hrefText.replaceAll("ref=category-tree", "limit=180&order=td&p=");

            list.add(hrefText);
        }

        return list;
    }

    private static List<String> getBigCategoriesList(String url) {
        List<String> list = new ArrayList<String>();
        String html = getPageHtml(url);
        int firstIndex = 0;
        int lastIndex = 0;
        String firstToken = "";
        String lastToken = "";

        firstToken = "<div class=\"main-nav clearfix\" data-url-root=\"/otdel/layer\">";
        lastToken = "<div class=\"nav-layer\" data-department=\"other\">";

        firstIndex = html.indexOf(firstToken, lastIndex);
        lastIndex = html.indexOf(lastToken, firstIndex);
        html = html.substring(firstIndex, lastIndex);

        firstIndex = 0;
        lastIndex = 0;
        firstToken = "";
        lastToken = "";

        while (true) {
            // поиск href

            firstToken = "<a class=\"nav-link\" href=\"";
            lastToken = "\"";
            firstIndex = html.indexOf(firstToken, lastIndex);

            if (firstIndex < 0) {
                break;
            }
            firstIndex = firstIndex + firstToken.length();
            lastIndex = html.indexOf(lastToken, firstIndex);
            String hrefText = html.substring(firstIndex, lastIndex);

            // поиск названию ссылки

            firstToken = "<strong>";
            lastToken = "</span>";
            firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
            lastIndex = html.indexOf(lastToken, firstIndex);
            String linkText = html.substring(firstIndex, lastIndex);

            linkText = linkText.replaceAll("<span>", "");
            if (hrefText.equals("http://molotok.ru/otdel/specpredlozenija/")) {
                continue;
            }
            list.add(hrefText);

        }

        return list;
    }

    public static String getPageHtml(String url) {
        String html = "";
        try {
            URL siteUrl = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) siteUrl.openConnection();
            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream, "UTF-8");
            html = scanner.useDelimiter("\\A").next();
            scanner.close();
        } catch (IOException e) {
            System.out.println("Connection timeout for " + url);
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            getPageHtml(url);
        } catch (NoSuchElementException e) {
            System.out.println(new Date());
            System.out.println("Connection ban for " + url);
            int sleepTime = new Random().nextInt(6);
            System.out.println("Sleep for " + sleepTime + " seconds");
            try {
                TimeUnit.SECONDS.sleep(sleepTime);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            System.out.println("Retrying ...");
            getPageHtml(url);
        }

        return html;
    }

}
