

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

class ItemFrequency {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Map<Long, Integer> hashMap = new HashMap<>();
        Set<Item> itemsList = new HashSet<>();
        try {
            FileInputStream fileInputStream = new FileInputStream("c:\\JAVA\\ItemIDCollector.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                long id = Long.parseLong(line);
                Integer frequency = hashMap.get(id);
                if (frequency == null) {
                    hashMap.put(id, 1);
                } else {
                    hashMap.put(id, frequency + 1);
                }
            }
            bufferedReader.close();
            System.out.println(hashMap.size() + " distinct words:");
            // System.out.println(sql);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Entry<Long, Integer>> list = new ArrayList<>(hashMap.entrySet());

        Collections.sort(list, (entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()));
        // String sql =
        // "INSERT INTO `items` (`item_id`, `title`, `price`, `frequency`, `image_url`, `amount`) VALUES \n\r";
        for (Entry<Long, Integer> each : list) {
            if (each.getValue() >= 3) {
                // http://molotok.ru/item4516468932_4516468932.html
                // System.out.print(each);
                Item item = getItemDescription(each.getKey());
                itemsList.add(item);
                // sql = sql + "(" + item.getItemId() + ", \"" + item.getTitle() + "\", " + item.getPrice() + ", " +
                // item.getFrequency() + ", '"
                // + item.getImageUrl() + "', " + item.getAmount() + "),\r\n";
                System.out.println("  " + item);

            }
        }

        Class.forName("com.mysql.jdbc.Driver");

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/molotok_analyzer", "root", "15348600");

        String sql = "INSERT INTO `items` (`item_id`, `title`, `categories`, `price`, `frequency`, `image_url`, `amount`) VALUES (?, ?, ?, ?, ? ,?, ?)";

        PreparedStatement ps = connection.prepareStatement(sql);
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {

            for (Item item : itemsList) {

                ps.setLong(1, item.getItemId());
                ps.setString(2, item.getTitle() + random.nextInt(20));
                ps.setInt(3, random.nextInt(9));
                ps.setInt(4, random.nextInt(10000));
                ps.setInt(5, random.nextInt(100));
                ps.setString(6, item.getImageUrl());
                ps.setInt(7, random.nextInt(70));
                ps.addBatch();

            }
            ps.executeBatch();
        }

        ps.executeBatch(); // insert remaining records
        ps.close();
        connection.close();

    }

    private static Item getItemDescription(long id) {

        String url = "http://molotok.ru/item" + id + "_" + id + ".html";
        System.out.println(url);
        String html = getPageHtml(url);
        // System.out.println(html);
        Item item = new Item();

        if (html.contains("Архив аукционов") || html.contains("Извините, страница аукциона перемещена в архив")) {
            return item;
        }

        int firstIndex = 0;
        int middleIndex;
        int lastIndex = 0;
        String firstToken;
        String middleToken;
        String lastToken;

        item.setItemId(id);

        // поиск itemId
        // data-itemdata='{"id":4545852620,"name":"\u017bWIREK SILIKONOWY DLA KOTA 8x 3.8L COMFY
        // PREMIUM","metaId":11116922,"prodId":0,
        // "photoLink":"http:\/\/img03.allegroimg.pl\/photos\/64x48\/45\/45\/85\/26\/4545852620","catId":90051,
        // "photoLinkLarge":"http:\/\/img03.allegroimg.pl\/photos\/128x96\/45\/45\/85\/26\/4545852620",
        // "buyNowPrice":"76,80 \u0440\u0443\u0431.",
        // "bidPrice":"76,80 \u0440\u0443\u0431.","freeShipping":1,"standard":0,"buyNowActive":true}'>

        firstToken = "<title>";
        lastToken = " (";
        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        lastIndex = html.indexOf(lastToken, firstIndex);
        String titleText = html.substring(firstIndex, lastIndex);
        item.setTitle(titleText);

        // поиск categories
        // <a href="/otdel/

        firstToken = "<a href=\"/otdel/";
        lastToken = "\"";
        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        lastIndex = html.indexOf(lastToken, firstIndex);
        String categories = html.substring(firstIndex, lastIndex);

        switch (categories) {
            case "elektronika-tehnika":
                categories = "elektronika-tehnika";
                break;
            case "telefony-aksessuary":
                categories = "telefony-aksessuary";
                break;
            case "iskusstvo-antikvariat":
                categories = "iskusstvo-antikvariat";
                break;
            case "kollekcionirovanie":
                categories = "kollekcionirovanie";
                break;
            case "moda-krasota":
                categories = "moda-krasota";
                break;
            case "muzyka-knigi-filmy":
                categories = "muzyka-knigi-filmy";
                break;
            case "dom-sport":
                categories = "dom-sport";
                break;
            case "avto-zapchasti":
                categories = "avto-zapchasti";
                break;
            case "vse-ostalnoe":
                categories = "vse-ostalnoe";
                break;
        }
        // item.setCategories(categories);

        // поиск imageUrl

        firstToken = "data-itemdata=";
        middleToken = "\"photoLink\":\"";
        lastToken = "\"";
        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        middleIndex = html.indexOf(middleToken, firstIndex) + middleToken.length();
        lastIndex = html.indexOf(lastToken, middleIndex);
        String imageUrlText = html.substring(middleIndex, lastIndex);
        item.setImageUrl(imageUrlText.replaceAll("\\\\", ""));

        // поиск price
        firstToken = "\"bidPrice\":\"";
        lastToken = ",";
        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        lastIndex = html.indexOf(lastToken, firstIndex);
        String priceText = html.substring(firstIndex, lastIndex);
        int price = Integer.parseInt(priceText.replaceAll(" ", ""));
        item.setPrice(price);

        // поиск frequency
        firstToken = "<span itemprop=\"offerCount\">";
        lastToken = "</span>";
        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        lastIndex = html.indexOf(lastToken, firstIndex);
        String frequencyText = html.substring(firstIndex, lastIndex);
        int frequency = Integer.parseInt(frequencyText);
        item.setFrequency(frequency);

        // поиск amount
        firstToken = "</strong>";
        middleToken = "</a>";
        lastToken = "</li>";
        firstIndex = html.indexOf(firstToken, lastIndex) + firstToken.length();
        middleIndex = html.indexOf(middleToken, firstIndex) + middleToken.length();
        lastIndex = html.indexOf(lastToken, middleIndex);
        String amountText = html.substring(middleIndex, lastIndex);
        if (amountText.contains("купил")) {
            firstToken = "<strong>";
            lastToken = "</strong>";
            firstIndex = amountText.indexOf(firstToken) + firstToken.length();
            lastIndex = amountText.indexOf(lastToken, firstIndex);
            String amountText2 = amountText.substring(firstIndex, lastIndex);

            int amount = Integer.parseInt(amountText2);
            item.setAmount(amount);

        } else {
            item.setFrequency(1);
            item.setAmount(1);
            return item;
        }

        return item;
    }

    private static String getPageHtml(String url) {
        String html = "";
        try {
            URL siteUrl = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) siteUrl.openConnection();
            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream, "UTF-8");
            html = scanner.useDelimiter("\\A").next();
            scanner.close();
        } catch (IOException e) {
            System.out.println("Connection timeout for " + url);
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            getPageHtml(url);
        } catch (NoSuchElementException e) {
            System.out.println(new Date());
            System.out.println("Connection ban for " + url);
            int sleepTime = new Random().nextInt(6);
            System.out.println("Sleep for " + sleepTime + " seconds");
            try {
                TimeUnit.SECONDS.sleep(sleepTime);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            System.out.println("Retrying ...");
            getPageHtml(url);
        }

        return html;
    }
}
